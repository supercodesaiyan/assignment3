Created the project using Node.js and Google Firebase.
This is a Magic8 game. You can ask queries to the game and it will answer you.
To download it to your system, clone the files to your local repository using the command below in the CLI:
$git clone https://bitbucket.org/supercodesaiyan/assignment3 .
Then, Install npm using:
$npm install
Start deploying the project using Firebase:
$firebase deploy


